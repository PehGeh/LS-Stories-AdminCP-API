"""A `game.ls-stories.de` account"""
type Account {
  id: Int!

  """The account's social club name"""
  socialClubName: String!

  """Whether the account is currently logged in to the gameserver"""
  loggedIn: Boolean

  """Account's admin level"""
  adminLevel: Int
  createdAt: String
  lastUsage: String
  lastIp: String
  lastHWID: String

  """Whether this account is locked out of the gameserver"""
  locked: Boolean

  """How many characters this account can have"""
  maxCharacters: Int

  """Account's characters"""
  characters: [Character]
}

"""A connection to a list of items."""
type AccountConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [AccountEdge]
}

"""An edge in a connection."""
type AccountEdge {
  """The item at the end of the edge"""
  node: Account

  """A cursor for use in pagination"""
  cursor: String!
}

"""A `game.ls-stories.de` character"""
type Character {
  id: Int!

  """The social club name this character belongs to"""
  socialClubName: String!
  createdAt: String
  lastUsage: String
  firstName: String
  lastName: String
  gender: Gender

  """Whether this character can be used or not"""
  locked: Boolean
  cash: Float
  bank: Float
  faction: Faction
  factionRank: Int
  posX: Float
  posY: Float
  posZ: Float
  hair: Int
  hairColor: Int
  hairHighlightColor: Int
  mother: Int
  father: Int
  similarity: Float
  skinSimilarity: Float
  eyebrows: Int
  eyebrowColor: Int
  eyeColor: Int
  beardColor: Int
  blushColor: Int
  lipstick: Int
  lipstickColor: Int
  chestHairColor: Int
  blemishes: Int
  facialhair: Int
  ageing: Int
  makeup: Int
  blush: Int
  complexion: Int
  sunDamage: Int
  freckles: Int
  chestHair: Int
  clothesTop: Int
  clothesLegs: Int
  clothesFeets: Int
  inventory: [InventoryItem]!
  hunger: Int
  thirst: Int
  boughtClothing: String
  onDuty: Int
  weapons: String
  licenses: [LicensesItem]!
  vehicles: [CharacterVehicle]!
  account: Account!
}

"""A `game.ls-stories.de` character vehicle"""
type CharacterVehicle {
  id: Int!
  engineHealth: Int
  tankLevel: Int
  faction: Faction
  numberPlate: String
  livery: Int
  primaryColor: Int
  secondaryColor: Int
  inUse: Boolean
  inventory: [InventoryItem]!
  inventoryWeight: Int
  vehicle: Vehicle!
  character: Character!
}

enum Faction {
  Citizen
  LSPD
  LSCS
  Police
  EMS
}

enum FuelType {
  Petrol
  Diesel
  Gas
  Electricity
  Kerosene
}

enum Gender {
  MALE
  FEMALE
}

type InventoryItem {
  quantity: Int!
  item: Item!
}

"""A `game.ls-stories.de` item"""
type Item {
  id: Int!
  name: String
  description: String
  type: ItemType
  weight: Int
  defaultPrice: Float
  defaultSellPrice: Float
  value1: Int
  value2: Int
  sellable: Boolean
}

"""A connection to a list of items."""
type ItemConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [ItemEdge]
}

"""An edge in a connection."""
type ItemEdge {
  """The item at the end of the edge"""
  node: Item

  """A cursor for use in pagination"""
  cursor: String!
}

enum ItemType {
  Special
  Food
  Drink
  Medic
  Repair
  Drug
  FuelCanister
}

"""A `game.ls-stories.de` license"""
type License {
  id: Int!
  name: String
  description: String
  type: LicenseType
}

type LicensesItem {
  issueDate: String
  license: License!
}

enum LicenseType {
  IdentityCard
  BusinessCard
  DrivingLicenseB
  DrivingLicenseC
  DrivingLicenseA
  DrivingLicenseK
  DrivingLicenseF
}

"""Global mutation object"""
type Mutation {
  """Authenticate"""
  auth(
    """`ls-stories.de` username"""
    username: String!

    """`ls-stories.de` password"""
    password: String!
  ): User

  """Logout"""
  logout: Boolean
}

"""Information about pagination in a connection."""
type PageInfo {
  """When paginating forwards, are there more items?"""
  hasNextPage: Boolean!

  """When paginating backwards, are there more items?"""
  hasPreviousPage: Boolean!

  """When paginating backwards, the cursor to continue."""
  startCursor: String

  """When paginating forwards, the cursor to continue."""
  endCursor: String
}

"""Global query object"""
type Query {
  """API Version"""
  version: String

  """That's you"""
  me: User
  accounts(after: String, first: Int, before: String, last: Int): AccountConnection
  account(
    """The account id"""
    id: Int!
  ): Account
  items(after: String, first: Int, before: String, last: Int): ItemConnection
  item(
    """The item id"""
    id: Int!
  ): Item
  vehicles(after: String, first: Int, before: String, last: Int, type: VehicleType): VehicleConnection
  vehicle(
    """The vehicle id"""
    id: Int!
  ): Vehicle
}

"""A `ls-stories.de` user"""
type User {
  username: String!
  socialClubName: String!
  adminLevel: Int!
  account: Account!
}

"""A `game.ls-stories.de` vehicle"""
type Vehicle {
  id: Int!
  type: VehicleType
  model: String
  displayName: String
  tankSize: Int
  fuelType: FuelType
  storageSize: Int
}

"""A connection to a list of items."""
type VehicleConnection {
  """Information to aid in pagination."""
  pageInfo: PageInfo!

  """A list of edges."""
  edges: [VehicleEdge]
}

"""An edge in a connection."""
type VehicleEdge {
  """The item at the end of the edge"""
  node: Vehicle

  """A cursor for use in pagination"""
  cursor: String!
}

enum VehicleType {
  Car
  Truck
  Motorcycle
  Helicopter
  Plane
  Boat
  Cycles
  Special
}
