import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import session from 'express-session';
import SessionStore from 'connect-session-knex';
import graphqlHTTP from 'express-graphql';

import { database } from './config/database';
import sessionConfig from './config/session';
import { sessionAuth } from './services/auth';
import schema from './schema';

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors({
  origin: process.env.NODE_ENV !== 'production' || 'https://dashboard.ls-stories.de',
  methods: ['GET', 'POST'],
  credentials: true,
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

app.use(helmet({
  dnsPrefetchControl: false,
  frameguard: false,
  ieNoOpen: false,
  hsts: process.env.NODE_ENV === 'production',
}));

app.use(session({
  ...sessionConfig,
  store: process.env.NODE_ENV === 'production' && new SessionStore(session)({
    knex: database,
  }),
}));

app.use(sessionAuth);

// GraphQL
app.use('/', graphqlHTTP({
  schema,
  graphiql: process.env.NODE_ENV !== 'production',
  pretty: process.env.NODE_ENV !== 'production',
  formatError: error => ({
    ...error,
    code: error.originalError && error.originalError.code,
  }),
}));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  if (!err.status) {
    err.status = 500; // eslint-disable-line no-param-reassign
  }

  res.locals.message = err.message;
  res.locals.error = (req.app.get('env') === 'development') ? err : { status: err.status };

  res.status(err.status);
  res.json(res.locals);
});

export default app;
