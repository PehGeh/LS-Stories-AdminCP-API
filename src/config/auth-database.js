const tables = {
  users: process.env.AUTH_DATABASE_TABLE_USERS,
  usersOptionValue: process.env.AUTH_DATABASE_TABLE_USERS_OPTION_VALUE,
};

const columns = {
  usersOption: process.env.AUTH_DATABASE_COLUMN_USERS_OPTION,
};

const database = require('knex')({
  client: 'mysql2',
  debug: process.env.NODE_ENV !== 'production',
  connection: {
    host: process.env.AUTH_DATABASE_HOST,
    user: process.env.AUTH_DATABASE_USER,
    password: process.env.AUTH_DATABASE_PASS,
    database: process.env.AUTH_DATABASE_NAME,
  },
  pool: {
    min: 0,
    max: 5,
  },
});

module.exports = {
  database,
  tables,
  columns,
};
