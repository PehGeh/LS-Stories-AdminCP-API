const dialect = 'mysql';
const tables = {
  accounts: 'accounts',
  characters: 'characters',
  characterVehicles: 'ownedvehicles',
  items: 'items',
  licenses: 'lizenzen',
  vehicles: 'vehicleinfo',
};

const database = require('knex')({
  client: 'mysql2',
  debug: process.env.NODE_ENV !== 'production',
  connection: {
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASS,
    database: process.env.DATABASE_NAME,
  },
  pool: {
    min: 0,
    max: 5,
  },
});

const dbCall = async sql => (await database.raw(sql))[0];

module.exports = {
  database,
  dialect,
  tables,
  dbCall,
};
