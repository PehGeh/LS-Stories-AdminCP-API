const secret = process.env.SESSION_SECRET;
if (!secret) {
  throw new Error('SESSION_SECRET not sepecfied');
}

module.exports = {
  secret,
  name: 'token',
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,
    sameSite: 'strict',
    secure: process.env.NODE_ENV === 'production',
  },
};
