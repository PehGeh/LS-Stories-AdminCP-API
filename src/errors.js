export class UnauthorizedError extends Error {
  code = 401;
  message = this.message || 'Unauthorized';
}

export class ForbiddenError extends Error {
  code = 403;
  message = this.message || 'Forbidden';
}
