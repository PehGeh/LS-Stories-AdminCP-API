import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInt,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';

import { tables } from '../config/database';

import Character from './Character';

export default new GraphQLObjectType({
  description: 'A `game.ls-stories.de` account',
  name: 'Account',
  sqlTable: tables.accounts,
  uniqueKey: 'Id',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      sqlColumn: 'Id',
    },

    socialClubName: {
      description: 'The account\'s social club name',
      type: new GraphQLNonNull(GraphQLString),
      sqlColumn: 'SocialClubName',
    },

    loggedIn: {
      description: 'Whether the account is currently logged in to the gameserver',
      type: GraphQLBoolean,
      sqlColumn: 'LoggedIn',
    },

    adminLevel: {
      description: 'Account\'s admin level',
      type: GraphQLInt,
      sqlColumn: 'AdminLvl',
    },

    createdAt: {
      type: GraphQLString,
      sqlColumn: 'CreatedAt',
    },

    lastUsage: {
      type: GraphQLString,
      sqlColumn: 'LastUsage',
    },

    lastIp: {
      type: GraphQLString,
      sqlColumn: 'LastIp',
    },

    lastHWID: {
      type: GraphQLString,
      sqlColumn: 'LastHWID',
    },

    locked: {
      description: 'Whether this account is locked out of the gameserver',
      type: GraphQLBoolean,
      sqlColumn: 'Locked',
    },

    maxCharacters: {
      description: 'How many characters this account can have',
      type: GraphQLInt,
      sqlColumn: 'MaxCharacters',
    },

    characters: {
      description: 'Account\'s characters',
      type: new GraphQLList(Character),
      sqlJoin: (accountsTable, charactersTable) =>
        `${accountsTable}.SocialClubName = ${charactersTable}.SocialClubName`,
      orderBy: 'Id',
    },
  },
});
