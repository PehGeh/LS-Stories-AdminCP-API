import {
  GraphQLObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';
import joinMonster from 'join-monster';

import { jsonToArray } from '../utils';
import { dialect, tables, dbCall } from '../config/database';

import License from './License';
import CharacterVehicle from './CharacterVehicle';
import Account from './Account';
import Faction from './Faction';
import InventoryItem from './InventoryItem';

const GenderType = new GraphQLEnumType({
  name: 'Gender',
  values: {
    MALE: { value: 0 },
    FEMALE: { value: 1 },
  },
});

const LicensesItemType = new GraphQLObjectType({
  name: 'LicensesItem',
  fields: {
    issueDate: {
      type: GraphQLString,
      resolve: ({ AusgestelltAm }) => AusgestelltAm,
    },

    license: {
      type: new GraphQLNonNull(License),
      where: (licensesTable, args, { type }) => `${licensesTable}.LicId = ${type}`,
      resolve: ({ LizenzId }, args, context, resolveInfo) =>
        joinMonster(resolveInfo, { type: LizenzId }, dbCall, { dialect }),
    },
  },
});

export default new GraphQLObjectType({
  description: 'A `game.ls-stories.de` character',
  name: 'Character',
  sqlTable: tables.characters,
  uniqueKey: 'Id',
  // This is a thunk, otherwise `Account` would be `undefined` (circular imports)
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      sqlColumn: 'Id',
    },

    socialClubName: {
      description: 'The social club name this character belongs to',
      type: new GraphQLNonNull(GraphQLString),
      sqlColumn: 'SocialClubName',
    },

    createdAt: {
      type: GraphQLString,
      sqlColumn: 'CreatedAt',
    },

    lastUsage: {
      type: GraphQLString,
      sqlColumn: 'LastUsage',
    },

    firstName: {
      type: GraphQLString,
      sqlColumn: 'FirstName',
    },

    lastName: {
      type: GraphQLString,
      sqlColumn: 'LastName',
    },

    gender: {
      type: GenderType,
      sqlColumn: 'Gender',
    },

    locked: {
      description: 'Whether this character can be used or not',
      type: GraphQLBoolean,
      sqlColumn: 'Locked',
    },

    cash: {
      type: GraphQLFloat,
      sqlColumn: 'Cash',
    },

    bank: {
      type: GraphQLFloat,
      sqlColumn: 'Bank',
    },

    faction: {
      type: Faction,
      sqlColumn: 'Faction',
    },

    factionRank: {
      type: GraphQLInt,
      sqlColumn: 'FactionRank',
    },

    posX: {
      type: GraphQLFloat,
      sqlColumn: 'PosX',
    },

    posY: {
      type: GraphQLFloat,
      sqlColumn: 'PosY',
    },

    posZ: {
      type: GraphQLFloat,
      sqlColumn: 'PosZ',
    },

    hair: {
      type: GraphQLInt,
      sqlColumn: 'Hair',
    },

    hairColor: {
      type: GraphQLInt,
      sqlColumn: 'HairColor',
    },

    hairHighlightColor: {
      type: GraphQLInt,
      sqlColumn: 'HairHighlightColor',
    },

    mother: {
      type: GraphQLInt,
      sqlColumn: 'Mother',
    },

    father: {
      type: GraphQLInt,
      sqlColumn: 'Father',
    },

    similarity: {
      type: GraphQLFloat,
      sqlColumn: 'Similarity',
    },

    skinSimilarity: {
      type: GraphQLFloat,
      sqlColumn: 'SkinSimilarity',
    },

    eyebrows: {
      type: GraphQLInt,
      sqlColumn: 'Eyebrows',
    },

    eyebrowColor: {
      type: GraphQLInt,
      sqlColumn: 'EyebrowColor',
    },

    eyeColor: {
      type: GraphQLInt,
      sqlColumn: 'EyeColor',
    },

    beardColor: {
      type: GraphQLInt,
      sqlColumn: 'BeardColor',
    },

    blushColor: {
      type: GraphQLInt,
      sqlColumn: 'BlushColor',
    },

    lipstick: {
      type: GraphQLInt,
      sqlColumn: 'Lipstick',
    },

    lipstickColor: {
      type: GraphQLInt,
      sqlColumn: 'LipstickColor',
    },

    chestHairColor: {
      type: GraphQLInt,
      sqlColumn: 'ChestHairColor',
    },

    blemishes: {
      type: GraphQLInt,
      sqlColumn: 'Blemishes',
    },

    facialhair: {
      type: GraphQLInt,
      sqlColumn: 'Facialhair',
    },

    ageing: {
      type: GraphQLInt,
      sqlColumn: 'Ageing',
    },

    makeup: {
      type: GraphQLInt,
      sqlColumn: 'Makeup',
    },

    blush: {
      type: GraphQLInt,
      sqlColumn: 'Blush',
    },

    complexion: {
      type: GraphQLInt,
      sqlColumn: 'Complexion',
    },

    sunDamage: {
      type: GraphQLInt,
      sqlColumn: 'Sundamage',
    },

    freckles: {
      type: GraphQLInt,
      sqlColumn: 'Freckles',
    },

    chestHair: {
      type: GraphQLInt,
      sqlColumn: 'Chesthair',
    },

    clothesTop: {
      type: GraphQLInt,
      sqlColumn: 'ClothesTop',
    },

    clothesLegs: {
      type: GraphQLInt,
      sqlColumn: 'ClothesLegs',
    },

    clothesFeets: {
      type: GraphQLInt,
      sqlColumn: 'ClothesFeets',
    },

    inventory: {
      type: new GraphQLNonNull(new GraphQLList(InventoryItem)),
      sqlColumn: 'Inventory',
      resolve: ({ inventory }) => jsonToArray(inventory),
    },

    inventoryWeight: {
      type: GraphQLFloat,
      sqlColumn: 'Cweight',
    },

    hunger: {
      type: GraphQLInt,
      sqlColumn: 'Hunger',
    },

    thirst: {
      type: GraphQLInt,
      sqlColumn: 'Thirst',
    },

    boughtClothing: {
      type: GraphQLString,
      sqlColumn: 'Bought_Clothing',
    },

    onDuty: {
      type: GraphQLInt,
      sqlColumn: 'OnDuty',
    },

    weapons: {
      type: GraphQLString,
      sqlColumn: 'Weapons',
    },

    licenses: {
      type: new GraphQLNonNull(new GraphQLList(LicensesItemType)),
      sqlColumn: 'Lizenzen',
      resolve: ({ licenses }) => jsonToArray(licenses),
    },

    vehicles: {
      type: new GraphQLNonNull(new GraphQLList(CharacterVehicle)),
      sqlJoin: (charactersTable, characterVehiclesTable) =>
        `${charactersTable}.Id = ${characterVehiclesTable}.OwnerCharId`,
    },

    account: {
      type: new GraphQLNonNull(Account),
      sqlJoin: (charactersTable, accountsTable) =>
        `${charactersTable}.SocialClubName = ${accountsTable}.SocialClubName`,
    },
  }),
});
