import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLInt,
  GraphQLFloat,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';

import { jsonToArray } from '../utils';
import { tables } from '../config/database';

import Faction from './Faction';
import InventoryItem from './InventoryItem';
import Vehicle from './Vehicle';
import Character from './Character';

export default new GraphQLObjectType({
  description: 'A `game.ls-stories.de` character vehicle',
  name: 'CharacterVehicle',
  sqlTable: tables.characterVehicles,
  uniqueKey: 'ID',
  // This is a thunk, otherwise `Character` would be `undefined` (circular imports)
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      sqlColumn: 'ID',
    },

    engineHealth: {
      type: GraphQLInt,
      sqlColumn: 'EngineHealth',
    },

    tankLevel: {
      type: GraphQLInt,
      sqlColumn: 'Fuel',
    },

    faction: {
      type: Faction,
      sqlColumn: 'Faction',
    },

    numberPlate: {
      type: GraphQLString,
      sqlColumn: 'NumberPlate',
    },

    livery: {
      type: GraphQLInt,
      sqlColumn: 'Livery',
    },

    primaryColor: {
      type: GraphQLInt,
      sqlColumn: 'PrimaryColor',
    },

    secondaryColor: {
      type: GraphQLInt,
      sqlColumn: 'SecondaryColor',
    },

    inUse: {
      type: GraphQLBoolean,
      sqlColumn: 'InUse',
    },

    inventory: {
      type: new GraphQLNonNull(new GraphQLList(InventoryItem)),
      sqlColumn: 'Inventory',
      resolve: ({ inventory }) => jsonToArray(inventory),
    },

    inventoryWeight: {
      type: GraphQLFloat,
      sqlColumn: 'Cladung',
    },

    vehicle: {
      type: new GraphQLNonNull(Vehicle),
      sqlJoin: (characterVehiclesTable, vehiclesTable) => `${characterVehiclesTable}.Model = ${vehiclesTable}.Model`,
    },

    character: {
      type: new GraphQLNonNull(Character),
      sqlJoin: (characterVehiclesTable, charactersTable) =>
        `${characterVehiclesTable}.OwnerCharId = ${charactersTable}.Id`,
    },
  }),
});
