import { GraphQLEnumType } from 'graphql';

export default new GraphQLEnumType({
  name: 'Faction',
  values: {
    Citizen: { value: 0 },
    LSPD: { value: 1 },
    LSCS: { value: 2 },
    Police: { value: 3 }, // LSPD & LSCS
    EMS: { value: 4 },
  },
});
