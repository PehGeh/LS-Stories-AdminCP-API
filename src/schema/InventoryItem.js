import { GraphQLObjectType, GraphQLNonNull, GraphQLInt } from 'graphql';
import joinMonster from 'join-monster';

import { dialect, dbCall } from '../config/database';

import Item from './Item';

export default new GraphQLObjectType({
  name: 'InventoryItem',
  fields: {
    quantity: {
      type: new GraphQLNonNull(GraphQLInt),
      resolve: ({ Count }) => Count,
    },

    item: {
      type: new GraphQLNonNull(Item),
      where: (itemsTable, args, { id }) => `${itemsTable}.Id = ${id}`,
      resolve: ({ ItemID }, args, context, resolveInfo) =>
        joinMonster(resolveInfo, { id: ItemID }, dbCall, { dialect }),
    },
  },
});
