import {
  GraphQLObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLFloat,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';

import { tables } from '../config/database';

const ItemType = new GraphQLEnumType({
  name: 'ItemType',
  values: {
    Special: { value: 0 },
    Food: { value: 1 },
    Drink: { value: 2 },
    Medic: { value: 3 },
    Repair: { value: 4 },
    Drug: { value: 5 },
    FuelCanister: { value: 6 },
  },
});

export default new GraphQLObjectType({
  description: 'A `game.ls-stories.de` item',
  name: 'Item',
  sqlTable: tables.items,
  uniqueKey: 'Id',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      sqlColumn: 'Id',
    },

    name: {
      type: GraphQLString,
      sqlColumn: 'Name',
    },

    description: {
      type: GraphQLString,
      sqlColumn: 'Description',
    },

    type: {
      type: ItemType,
      sqlColumn: 'Type',
    },

    weight: {
      type: GraphQLFloat,
      sqlColumn: 'Weight',
    },

    defaultPrice: {
      type: GraphQLFloat,
      sqlColumn: 'DefaultPrice',
    },

    defaultSellPrice: {
      type: GraphQLFloat,
      sqlColumn: 'DefaultSellPrice',
    },

    value1: {
      type: GraphQLInt,
      sqlColumn: 'Value1',
    },

    value2: {
      type: GraphQLInt,
      sqlColumn: 'Value2',
    },

    sellable: {
      type: GraphQLBoolean,
      sqlColumn: 'Sellable',
    },
  },
});
