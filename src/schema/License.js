import {
  GraphQLObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';

import { tables } from '../config/database';

const LicenseType = new GraphQLEnumType({
  name: 'LicenseType',
  values: {
    IdentityCard: { value: 0 }, // Nice license type :D
    BusinessCard: { value: 1 }, // Yep, business cards are licenses as well!?! o_O
    DrivingLicenseB: { value: 2 },
    DrivingLicenseC: { value: 3 },
    DrivingLicenseA: { value: 4 },
    DrivingLicenseK: { value: 5 },
    DrivingLicenseF: { value: 6 },
  },
});

export default new GraphQLObjectType({
  description: 'A `game.ls-stories.de` license',
  name: 'License',
  sqlTable: tables.licenses,
  uniqueKey: 'Id',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      sqlColumn: 'Id',
    },

    name: {
      type: GraphQLString,
      sqlColumn: 'Name',
    },

    description: {
      type: GraphQLString,
      sqlColumn: 'Description',
    },

    type: {
      type: LicenseType,
      sqlColumn: 'LicId',
    },
  },
});
