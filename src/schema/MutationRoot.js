import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLBoolean,
} from 'graphql';

import { auth, requiresAuth } from '../services/auth';

import User from './User';

export default new GraphQLObjectType({
  description: 'Global mutation object',
  name: 'Mutation',
  fields: {
    auth: {
      description: 'Authenticate',
      type: User,
      args: {
        username: {
          description: '`ls-stories.de` username',
          type: new GraphQLNonNull(GraphQLString),
        },
        password: {
          description: '`ls-stories.de` password',
          type: new GraphQLNonNull(GraphQLString),
        },
      },
      resolve: (parent, { username, password }, context) => auth(context, username, password),
    },

    logout: {
      description: 'Logout',
      type: GraphQLBoolean,
      resolve: requiresAuth((paremt, args, { session }) => {
        if (session) {
          session.destroy();
        }

        return true;
      }),
    },
  },
});
