import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
} from 'graphql-relay';
import joinMonster from 'join-monster';

import { version } from '../../package.json';
import { dialect, dbCall } from '../config/database';
import { requiresAuth } from '../services/auth';

import User from './User';
import Account from './Account';
import Item from './Item';
import Vehicle, { VehicleType } from './Vehicle';

const { connectionType: AccountConnection } = connectionDefinitions({ nodeType: Account });
const { connectionType: ItemConnection } = connectionDefinitions({ nodeType: Item });
const { connectionType: VehicleConnection } = connectionDefinitions({ nodeType: Vehicle });

export default new GraphQLObjectType({
  description: 'Global query object',
  name: 'Query',
  fields: {
    version: {
      description: 'API Version',
      type: GraphQLString,
      resolve: () => version,
    },

    viewer: {
      description: 'That\'s you',
      type: User,
      resolve: requiresAuth((parent, args, { user }) => user),
    },

    accounts: {
      type: AccountConnection,
      args: connectionArgs,
      orderBy: 'Id',
      resolve: requiresAuth((parent, args, context, resolveInfo) =>
        joinMonster(resolveInfo, context, dbCall, { dialect })
          .then(data => connectionFromArray(data, args))),
    },

    account: {
      type: Account,
      args: {
        id: {
          description: 'The account id',
          type: new GraphQLNonNull(GraphQLInt),
        },
      },
      where: (accountsTable, { id }) => `${accountsTable}.Id = ${id}`,
      resolve: requiresAuth((parent, args, context, resolveInfo) =>
        joinMonster(resolveInfo, context, dbCall, { dialect })),
    },

    items: {
      type: ItemConnection,
      args: connectionArgs,
      orderBy: 'Id',
      resolve: requiresAuth((parent, args, context, resolveInfo) =>
        joinMonster(resolveInfo, context, dbCall, { dialect })
          .then(data => connectionFromArray(data, args))),
    },

    item: {
      type: Item,
      args: {
        id: {
          description: 'The item id',
          type: new GraphQLNonNull(GraphQLInt),
        },
      },
      where: (itemsTable, { id }) => `${itemsTable}.Id = ${id}`,
      resolve: requiresAuth((parent, args, context, resolveInfo) =>
        joinMonster(resolveInfo, context, dbCall, { dialect })),
    },

    vehicles: {
      type: VehicleConnection,
      args: {
        ...connectionArgs,
        type: {
          type: VehicleType,
        },
      },
      orderBy: 'Id',
      where: (vehiclesTable, { type }) => VehicleType.serialize(type) && `${vehiclesTable}.type = ${type}`,
      resolve: requiresAuth((parent, args, context, resolveInfo) =>
        joinMonster(resolveInfo, context, dbCall, { dialect })
          .then(data => connectionFromArray(data, args))),
    },

    vehicle: {
      type: Vehicle,
      args: {
        id: {
          description: 'The vehicle id',
          type: new GraphQLNonNull(GraphQLInt),
        },
      },
      where: (vehiclesTable, { id }) => `${vehiclesTable}.Id = ${id}`,
      resolve: requiresAuth((parent, args, context, resolveInfo) =>
        joinMonster(resolveInfo, context, dbCall, { dialect })),
    },
  },
});
