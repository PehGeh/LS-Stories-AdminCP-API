import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';
import joinMonster from 'join-monster';

import { database, dialect, dbCall } from '../config/database';

import Account from './Account';

export default new GraphQLObjectType({
  description: 'A `ls-stories.de` user',
  name: 'User',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
    },

    username: {
      type: new GraphQLNonNull(GraphQLString),
    },

    socialClubName: {
      type: new GraphQLNonNull(GraphQLString),
    },

    adminLevel: {
      type: new GraphQLNonNull(GraphQLInt),
    },

    account: {
      type: new GraphQLNonNull(Account),
      where: (accountsTable, args, { user }) =>
        `${accountsTable}.SocialClubName = ${database.raw('?', user.socialClubName)}`,
      resolve: (parent, args, context, resolveInfo) => joinMonster(resolveInfo, context, dbCall, { dialect }),
    },
  },
});
