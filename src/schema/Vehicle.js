import {
  GraphQLObjectType,
  GraphQLEnumType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLString,
} from 'graphql';

import { tables } from '../config/database';

export const VehicleType = new GraphQLEnumType({
  name: 'VehicleType',
  values: {
    Car: { value: 0 },
    Truck: { value: 1 },
    Motorcycle: { value: 2 },
    Helicopter: { value: 3 },
    Plane: { value: 4 },
    Boat: { value: 5 },
    Cycles: { value: 6 },
    Special: { value: 7 },
  },
});

const FuelType = new GraphQLEnumType({
  name: 'FuelType',
  values: {
    Petrol: { value: 0 },
    Diesel: { value: 1 },
    Gas: { value: 2 },
    Electricity: { value: 3 },
    Kerosene: { value: 4 },
  },
});

export default new GraphQLObjectType({
  description: 'A `game.ls-stories.de` vehicle',
  name: 'Vehicle',
  sqlTable: tables.vehicles,
  uniqueKey: 'Id',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      sqlColumn: 'Id',
    },

    type: {
      type: VehicleType,
      sqlColumn: 'Type',
    },

    model: {
      type: GraphQLString,
      sqlColumn: 'Model',
    },

    displayName: {
      type: GraphQLString,
      sqlColumn: 'DisplayName',
    },

    tankSize: {
      type: GraphQLInt,
      sqlColumn: 'maxFuel',
    },

    fuelType: {
      type: FuelType,
      sqlColumn: 'Fuel',
    },

    storageSize: {
      type: GraphQLInt,
      sqlColumn: 'MaxStorage',
    },
  },
});
