import bcrypt from 'bcrypt';

import { UnauthorizedError, ForbiddenError } from '../errors';
import { database, tables } from '../config/database';
import {
  database as authDatabase,
  tables as authTables,
  columns as authColumns,
} from '../config/auth-database';

const getDoubleHashedSalt = (password, salt) => bcrypt.hash(password, salt);

const fetchUser = username => authDatabase
  .select(
    `${authTables.users}.userId`,
    `${authTables.users}.username`,
    `${authTables.users}.password`,
    `${authTables.usersOptionValue}.${authColumns.usersOption} AS socialClubName`,
  )
  .from(authTables.users)
  .where('username', username)
  .innerJoin(authTables.usersOptionValue, `${authTables.usersOptionValue}.userId`, `${authTables.users}.userId`)
  .first();

const fetchAccount = socialClubName => database
  .select('AdminLvL AS adminLevel')
  .from(tables.accounts)
  .where('SocialClubName', socialClubName)
  .first();

export const auth = async (context, username, password) => {
  if (context.user || (context.session && context.session.username)) {
    throw new Error('Already authenticated');
  }

  const user = await fetchUser(username);
  if (!user || !user.userId || !user.username || !user.password) {
    throw new Error('Unknown username');
  }

  if (!user.socialClubName) {
    throw new Error('You have to set your Social Club Name at `ls-stories.de`');
  }

  // Woltlabs way
  const hashedPassword = await getDoubleHashedSalt(password, user.password);
  if (!await bcrypt.compare(hashedPassword, user.password)) {
    throw new Error('Wrong password');
  }

  const account = await fetchAccount(user.socialClubName);
  if (!account || !account.adminLevel) {
    throw new Error('You do not have enough power');
  }

  /* eslint-disable no-param-reassign */
  context.session.username = user.username;
  context.session.hashedPassword = hashedPassword;

  context.user = {
    id: user.userId,
    username: user.username,
    socialClubName: user.socialClubName,
    adminLevel: account.adminLevel,
  };
  /* eslint-enable no-param-reassign */

  return context.user;
};

export const sessionAuth = async (req, res, next) => {
  if (!req.user && req.session) {
    const { username, hashedPassword } = req.session;

    if (username && hashedPassword) {
      const user = await fetchUser(username);

      if (user && user.username && user.password && user.socialClubName &&
        await bcrypt.compare(hashedPassword, user.password)
      ) {
        const account = await fetchAccount(user.socialClubName);

        if (account && account.adminLevel) {
          req.user = {
            id: user.userId,
            username: user.username,
            socialClubName: user.socialClubName,
            adminLevel: account.adminLevel,
          };

          next();
          return;
        }
      }

      req.session.destroy();
    }
  }

  next();
};

export const requiresAuth = next => (parent, args, context, resolveInfo) => {
  const { user } = context;
  if (!user || !user.adminLevel) {
    throw new UnauthorizedError();
  }

  return next(parent, args, context, resolveInfo);
};

export const requiresLevel = (level, next) => (parent, args, context, resolveInfo) => {
  const { user } = context;
  if (!user || !user.adminLevel) {
    throw new UnauthorizedError();
  }

  if (user.adminLevel < level) {
    throw new ForbiddenError();
  }

  return next(parent, args, context, resolveInfo);
};
