const jsonToArray = (json) => {
  if (json) {
    try {
      const array = JSON.parse(json);
      if (Array.isArray(array)) {
        return array;
      }
    } catch (error) {} // eslint-disable-line no-empty
  }

  return [];
};

export default {
  jsonToArray,
};
